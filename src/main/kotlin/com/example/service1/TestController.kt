package com.example.service1

import com.example.service1.config.TestConfig
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class TestController(
    private val testConfig: TestConfig
) {

    @GetMapping("/test")
    fun getConfigData(): String {
        return String.format("name: %s, phone: %s", testConfig.name, testConfig.phone)
    }

}