package com.example.service1.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.stereotype.Component

/* bean that handle properties starting with "test" */
@Component
@ConfigurationProperties("test")
@RefreshScope
class TestConfig {
    lateinit var name: String
    lateinit var phone: String
}