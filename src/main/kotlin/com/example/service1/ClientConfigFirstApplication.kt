package com.example.service1

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@SpringBootApplication
@EnableConfigurationProperties
@EnableEurekaClient
class ClientConfigFirstApplication

fun main(args: Array<String>) {
    runApplication<ClientConfigFirstApplication>(*args)
}
